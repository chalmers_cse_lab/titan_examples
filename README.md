This is a collection of scripts, instructions, and some code for running jobs on titan.

For instructions on how to run them go to the [**Titan Wiki**](https://bitbucket.org/chalmers_cse_lab/titan_examples/wiki/).
 
Good Luck!
