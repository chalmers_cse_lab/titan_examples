#!/bin/bash

#create log dir, if it doesn't aready exist.
[ -d log ] || mkdir log 

# Build images using current state of the repository
docker build -t kageback/exempel:gpu . &&\
#submit job, asking for one GPU, and redirecting output to log files
qsub -cwd -l gpu=1 -b y \
  -e ./log/bash_job.error \
  -o ./log/bash_job.log \
  nvidia-docker run -t --rm kageback/exempel:gpu python -u ./job_code.py

